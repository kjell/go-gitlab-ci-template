// Simple example appliaction using gitlab ci.
//
// see http://kjell.gitlab.io/go-gitlab-ci-template/
package main

import (
	"fmt"

	"gitlab.com/kjell/go-gitlab-ci-template/world"
)

var version, build string

func main() {
	n := world.Name("world")
	fmt.Println(n)

	fmt.Println("Version: ", version)
	fmt.Println("build: ", build)
}

func foo() string {
	return "bar"
}
