package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFoo(t *testing.T) {
	Convey("foo() should return 'bar'", t, func() {
		So(foo(), ShouldEqual, "bar")
	})
}
