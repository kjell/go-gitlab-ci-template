// Package world is a demonstration of documentation of a package.
//
// The documentation is available at:
// http://kjell.gitlab.io/go-gitlab-ci-template
//
// For more godoc syntax, take a look at:
// https://godoc.org/github.com/fluhus/godoc-tricks
package world

import "fmt"

// Name to greet
type Name string

// Greet n, Name
func (n Name) Greet() string {
	return fmt.Sprintf("%s", n)
}

func (n Name) String() string {
	return fmt.Sprintf("Hello %s", string(n))
}
