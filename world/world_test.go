package world

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGreet(t *testing.T) {
	Convey("greet() should return string", t, func() {
		n := Name("foo")
		So(n.Greet(), ShouldEqual, "Hello foo")
	})
}
