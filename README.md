# gitlab ci template

[![build status](https://gitlab.com/kjell/go-gitlab-ci-template/badges/master/build.svg)](https://gitlab.com/kjell/go-gitlab-ci-template/pipelines?scope=branches)
[![coverage report](https://gitlab.com/kjell/go-gitlab-ci-template/badges/master/coverage.svg)](http://kjell.gitlab.io/go-gitlab-ci-template/cover.html)
[![Build status](https://ci.appveyor.com/api/projects/status/go3sgx4252pa2602/branch/master?svg=true)](https://ci.appveyor.com/project/kjellkvinge/go-gitlab-ci-template/branch/master)


## [http://kjell.gitlab.io/go-gitlab-ci-template/](http://kjell.gitlab.io/go-gitlab-ci-template/)

This template should be enough to create a project with automatic test
and build using gitlabs CI system.

See documentation for the ci config file, [.gitlab-ci.yml](.gitlab-ci.yml)
docs: https://docs.gitlab.com/ee/ci/yaml/


## builds

For releases: see [tags](https://gitlab.com/kjell/go-gitlab-ci-template/tags)

latest:

* [linux-amd64](https://gitlab.com/kjell/go-gitlab-ci-template/builds/artifacts/master/download?job=linux-amd64)
* [mac (darwin-amd64)](https://gitlab.com/kjell/go-gitlab-ci-template/builds/artifacts/master/download?job=darwin-amd64)
* [windows](https://ci.appveyor.com/api/projects/kjellkvinge/go-gitlab-ci-template/artifacts/myapp)
